use std::sync;
use std::thread;
use tracing::trace;
use tracing_subscriber::{filter, fmt, prelude::*, EnvFilter};

mod cmd;
mod http;
mod jsonrpc;

struct TestHandler {
    tx: sync::mpsc::Sender<(Vec<i64>, sync::mpsc::Sender<i64>)>,
}

impl TestHandler {
    fn new(tx: sync::mpsc::Sender<(Vec<i64>, sync::mpsc::Sender<i64>)>) -> TestHandler {
        TestHandler { tx }
    }
}

impl cmd::Handler for TestHandler {
    // Compute sum of values by sending its inputs ot another thread.
    fn sum(&self, request: &cmd::CmdSumRequest) -> cmd::CmdSumResponse {
        let (tx, rx) = sync::mpsc::channel();

        trace!("send values to computing side");
        self.tx.send((request.values.clone(), tx)).unwrap();

        trace!("wait for result");
        cmd::CmdSumResponse {
            sum: rx.recv().unwrap(),
        }
    }
}

fn main() {
    tracing_subscriber::registry()
        // Read filter from RUST_LOG.
        .with(
            EnvFilter::builder()
                // Use INFO level when it is not set from env variable.
                .with_default_directive(filter::LevelFilter::INFO.into())
                // Ignore error during parsing directive.
                .from_env_lossy(),
        )
        // Set output format (can add `.json()` to output JSON format, or).
        .with(fmt::layer())
        // Install this Subscriber as the global default.
        .init();

    let (tx, rx) = sync::mpsc::channel();

    // Start tokio's entry function to handle HTTP requests in another thread.
    let handler = thread::spawn(|| {
        http::serve(
            3030,
            sync::Arc::new(sync::Mutex::new(Box::new(TestHandler::new(tx)))),
        );
    });

    // Receive message, compute sum then send it back.
    while let Ok((v, tx)) = rx.recv() {
        trace!("compute sum and send the result back");
        tx.send(v.iter().sum()).unwrap();
    }

    handler.join().unwrap();
}
