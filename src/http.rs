use crate::cmd;
use crate::jsonrpc;
use serde::{de::DeserializeOwned, Serialize};
use std::sync;
use tracing::{debug, info, instrument, span, warn, Level};
use warp::Filter;

// A helper function to reduce boilerplate.
fn call<Request, Response, F>(
    params: &serde_json::Value,
    op: F,
) -> Result<serde_json::Value, jsonrpc::ResponseError>
where
    Request: DeserializeOwned,
    Response: Serialize,
    F: FnOnce(&Request) -> Response,
{
    serde_json::from_value(params.clone())
        .map_err(|_| jsonrpc::ResponseError::new_std(jsonrpc::StdErrorCode::InvalidParams))
        .map(|v| {
            debug!("call function");
            op(&v)
        })
        .map(serde_json::to_value)?
        .map_err(|_| jsonrpc::ResponseError::new_std(jsonrpc::StdErrorCode::InternalError))
}

#[instrument(skip_all)]
fn rpc(
    handler: sync::Arc<sync::Mutex<Box<dyn cmd::Handler>>>,
    request: jsonrpc::Request,
) -> warp::reply::Json {
    // Check json-rpc version.
    if request.jsonrpc != "2.0" {
        info!(version = request.jsonrpc, "invalid json-rpc version");
        return warp::reply::json(&jsonrpc::Response::new_err(
            request.id,
            jsonrpc::ResponseError::new_std(jsonrpc::StdErrorCode::InvalidRequest),
        ));
    }

    let result = if let Ok(ref mut h) = handler.try_lock() {
        // Create a new span that wraps all logging in this scope.
        // The level of span determines whether its name is included
        // in the output log, and does not affect level of any logs
        // in its scope.
        let _span = span!(Level::DEBUG, "call rpc method").entered();
        match request.method.as_str() {
            "echo" => call(&request.params, |a| h.echo(a)),
            "sum" => call(&request.params, |a| h.sum(a)),

            _ => {
                warn!("invalid method");
                Err(jsonrpc::ResponseError::new_std(
                    jsonrpc::StdErrorCode::MethodNotFound,
                ))
            }
        }
    } else {
        warn!("could not lock handler");
        Err(jsonrpc::ResponseError::new_std(
            jsonrpc::StdErrorCode::ServerBusy,
        ))
    };

    warp::reply::json(&jsonrpc::Response::new(request.id, result.into()))
}

#[tokio::main]
pub async fn serve(port: u16, handler: sync::Arc<sync::Mutex<Box<dyn cmd::Handler>>>) {
    let ping = warp::get().and(warp::path!("ping")).map(warp::reply);
    let jsonrpc = warp::post()
        .and(warp::path!("api" / "jsonrpc-v2" / "v1"))
        .and(warp::body::content_length_limit(16 * 1024))
        .and(warp::body::json())
        .map(move |request: jsonrpc::Request| rpc(handler.clone(), request));
    let help = warp::get()
        .and(warp::path!("help"))
        .map(move ||
            [
                "GET  /help",
                "GET  /ping",
                "POST /api/jsonrpc-v2/v1",
                "---",
                &format!("curl http://127.0.0.1:{}/help", port),
                &format!("curl http://127.0.0.1:{}/ping", port),
                &format!(r#"curl http://127.0.0.1:{}/api/jsonrpc-v2/v1 -X POST -H "Content-Type: application/json" -d '{{"jsonrpc":"2.0", "method":"echo", "params": {{"text": "Test String"}}, "id": "cmFuZG9t"}}'"#, port),
                &format!(r#"curl http://127.0.0.1:{}/api/jsonrpc-v2/v1 -X POST -H "Content-Type: application/json" -d '{{"jsonrpc":"2.0", "method":"sum", "params": {{"values": [-1, 0, 1, 2, 3, 4, 5]}}, "id": "ODE3MA=="}}'"#, port),
            ].join("\n")
        );

    let routes = ping.or(help).or(jsonrpc);

    warp::serve(routes).run(([127, 0, 0, 1], port)).await;
}
