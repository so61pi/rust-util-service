use serde::{Deserialize, Serialize};
use strum::Display;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub enum RequestId {
    String(String),
    Num(i64),
    Null,
}

#[derive(Debug, Deserialize)]
pub struct Request {
    pub jsonrpc: String,
    pub id: RequestId,
    pub method: String,
    #[serde(default = "serde_json::Value::default")]
    #[serde(skip_serializing_if = "serde_json::Value::is_null")]
    pub params: serde_json::Value,
}

#[derive(Debug, Serialize)]
pub struct Response {
    jsonrpc: String,
    id: RequestId,
    result: ResponseResult,
}

impl Response {
    pub fn new_ok(id: RequestId, result: serde_json::Value) -> Response {
        Response::new(id, ResponseResult::Ok(result))
    }

    pub fn new_err(id: RequestId, err: ResponseError) -> Response {
        Response::new(id, ResponseResult::Err(err))
    }

    pub fn new(id: RequestId, result: ResponseResult) -> Response {
        Response {
            jsonrpc: String::from("2.0"),
            id,
            result,
        }
    }
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum ResponseResult {
    Ok(serde_json::Value),
    Err(ResponseError),
}

impl From<Result<serde_json::Value, ResponseError>> for ResponseResult {
    fn from(result: Result<serde_json::Value, ResponseError>) -> ResponseResult {
        match result {
            Ok(v) => ResponseResult::Ok(v),
            Err(e) => ResponseResult::Err(e),
        }
    }
}

#[derive(Debug, Serialize)]
pub struct ResponseError {
    code: i32,
    #[serde(skip_serializing_if = "String::is_empty")]
    message: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    data: Option<serde_json::Value>,
}

impl ResponseError {
    pub fn new(code: i32, message: String) -> ResponseError {
        // JSON-RPC: The error codes from and including -32768 to -32000 are reserved for pre-defined errors.
        if (-32768..=-32000).contains(&code) {
            panic!("cannot use preserved error codes");
        }
        ResponseError {
            code,
            message,
            data: None,
        }
    }

    pub fn new_std(code: StdErrorCode) -> ResponseError {
        ResponseError {
            code: code as i32,
            message: code.to_string(),
            data: None,
        }
    }

    pub fn with_err(self, err: Option<serde_json::Value>) -> ResponseError {
        ResponseError {
            code: self.code,
            message: self.message,
            data: err,
        }
    }
}

#[derive(Display, Clone, Copy)]
pub enum StdErrorCode {
    // JSON-RPC: The error codes from and including -32768 to -32000 are reserved for pre-defined errors.
    #[strum(serialize = "Parse error")]
    ParseError = -32700,

    #[strum(serialize = "Invalid request")]
    InvalidRequest = -32600,

    #[strum(serialize = "Method not found")]
    MethodNotFound = -32601,

    #[strum(serialize = "Invalid params")]
    InvalidParams = -32602,

    #[strum(serialize = "Internal error")]
    InternalError = -32603,

    // JSON-RPC: -32000 to -32099: Server error (implementation-defined server-errors)
    #[strum(serialize = "Server is busy")]
    ServerBusy = -32000,
}
