use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct CmdEchoRequest {
    pub text: String,
}

#[derive(Debug, Serialize)]
pub struct CmdEchoResponse {
    pub text: String,
}

#[derive(Debug, Deserialize)]
pub struct CmdSumRequest {
    pub values: Vec<i64>,
}

#[derive(Debug, Serialize)]
pub struct CmdSumResponse {
    pub sum: i64,
}

pub trait Handler: Send {
    fn echo(&self, request: &CmdEchoRequest) -> CmdEchoResponse {
        CmdEchoResponse {
            text: request.text.clone(),
        }
    }

    fn sum(&self, request: &CmdSumRequest) -> CmdSumResponse {
        CmdSumResponse {
            sum: request.values.iter().sum(),
        }
    }
}
