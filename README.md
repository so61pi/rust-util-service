## Use This Project As Template

Please follow https://github.com/cargo-generate/cargo-generate to generate a new project using this one as template.

## Frequently Used Commands

```sh
# Format
cargo fmt

# Check
cargo check
cargo clippy

# Build
cargo build
cargo build --release

# Run
cargo run
# with debug log level
RUST_LOG=debug cargo run
# with info log level by default and trace log level for hyper
RUST_LOG=info,hyper=trace cargo run

# Run tests or benchmarks
cargo test
cargo bench
```

Use `curl http://127.0.0.1:3030/help` to get help.

## Run With Miri

Miri is `an experimental interpreter for Rust's mid-level intermediate representation (MIR). It can run binaries and test suites of cargo projects and detect certain classes of undefined behavior`. It is desired to run tests and/or program with Miri to detect potential bugs.

Follow this link https://github.com/rust-lang/miri#using-miri to install Miri.

```sh
# Run program with Miri
cargo +nightly miri run

# Run tests with Miri
cargo +nightly miri test
```
